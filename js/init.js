// gobal vars needed for various game functions
var boxes, bombs, checkedBoxes, timer, totalTime, isTimerRunning;

$(window).load(function(){
	newGame('normal');

	// user clicks one of the 3 difficulty buttons
	$('button[data-difficulty]').on('click', function(){
		// no need to reset the game if someone clicks on the active difficulty
		if ( $(this).hasClass('active') ) {
			return false;
		}

		$('button[data-difficulty]').removeClass('active');
		$(this).addClass('active');
		resetGame();
	});

	// user clicks Reset Game button
	$('#resetGame').on('click', resetGame = function() {
		stopTimer();
		var difficulty = $('button[data-difficulty].active').data( 'difficulty');
		newGame(difficulty);
	});
});

// auto-adjust box heights on window resize
$(window).resize(boxAutoHeight);