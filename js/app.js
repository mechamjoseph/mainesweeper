
function newGame(difficulty) {

	// reset game vars
	var bombsSet = 0;
	boxes = []; /* declared in init.js */
	bombs = []; /* declared in init.js */
	checkedBoxes = []; /* declared in init.js */
	$('#timer').html('0:00');
	totalTime = 0;
	isTimerRunning = false;
	$('.alert').slideUp('fast');

	var boxesHtml = '';
	for (var i = 0; i < 81; i++) {
		// create boxes divs
		boxesHtml += '<div class="ms-box" data-boxid="'+i+'"></div>';

		boxes[i] = {
			'adjacentBoxes': calcAdjacent(i),
			'isBomb': false
		}
	}

	$('#game-board').html( boxesHtml );

	// this setTimeout helps with an issue where boxAutoHeight was executing before the boxesHtml was in place
	setTimeout(boxAutoHeight, 0);

	switch (difficulty) {
		case 'easy':
			numBombs = 5;
			break;
		case 'normal':
			numBombs = 10;
			break;
		case 'hard':
			numBombs = 15;
			break;
	}

	// set bombs
	for ( var i = 0; i < numBombs; i++ ) {
		randomNum = Math.floor((Math.random() * 81));
		// ensure no duplicates
		if ( boxes[randomNum].isBomb ) {
			i--;
			continue;
		}
		boxes[randomNum].isBomb = true;
		bombs.push(randomNum);
	}

	// all boxes are displayed and bombs set. watch for user interaction.
	$('.ms-box').on('click',boxClick);

	// this function is nested inside newGame() because it's the only place it's needed
	function calcAdjacent(boxId) {

		// check if top box
		var isTopBox = false;
		if ( boxId <= 8 ) {
			isTopBox = true;
		}

		// check if right box
		var isRightBox = false;
		if ( boxId % 9 == 8 ) {
			isRightBox = true;
		}

		// check if bottom box
		var isBottomBox = false;
		if ( boxId >= 72 ) {
			isBottomBox = true;
		}

		// check if left box
		var isLeftBox = false;
		if ( boxId % 9 == 0 ) {
			isLeftBox = true;
		}

		var adjacentBoxes = [];

		// apply adjacent boxes where applicable
		if (!isTopBox)		adjacentBoxes.push(boxId-9);
		if (!isRightBox)	adjacentBoxes.push(boxId+1);
		if (!isBottomBox)	adjacentBoxes.push(boxId+9);
		if (!isLeftBox)		adjacentBoxes.push(boxId-1);
		if (!isLeftBox && !isTopBox)		adjacentBoxes.push(boxId-10);
		if (!isTopBox && !isRightBox)		adjacentBoxes.push(boxId-8);
		if (!isRightBox && !isBottomBox)	adjacentBoxes.push(boxId+10);
		if (!isBottomBox && !isLeftBox)		adjacentBoxes.push(boxId+8);

		return adjacentBoxes;
	}
}


function boxClick(event) {
	var box = $(event.target);
	// fixes an issue where the "span" was being selected instead of the box div and breaking later
	if( !box.hasClass('ms-box') || box.hasClass('no-touchy')) {
		return false;
	}

	var boxId = box.data('boxid');
	var boxData = boxes[ boxId ];

	// box was a bomb
	if ( boxData.isBomb ) {
		stopTimer();
		$('.ms-box').addClass('no-touchy');
		showBombs('lose');
		box.addClass('explode');
		$('.alert-danger').slideDown('fast');
		return false;
	}

	// wasn't a bomb. mark the box as checked.
	checkedBoxes.push(boxId);

	// start game timer
	if ( !isTimerRunning ) {
		startTimer();
	}

	// check for adjacent bombs
	var adjacentBombs = 0;
	$.each( boxData.adjacentBoxes, function(_, adjacentBoxId ){
		if ( boxes[adjacentBoxId].isBomb ) {
			adjacentBombs++;
		}
	});

	// box has bomb(s) adjacent to it
	if ( adjacentBombs >= 1 ) {
		box.addClass('number');
		box.html('<span>'+adjacentBombs+'</span>');
	}
	else {
		// box is clear
		box.addClass('clear');
		$.each( boxData.adjacentBoxes, function(_, adjacentBoxId ){
			// don't keep rechecking same boxes
			if ( checkedBoxes.indexOf(adjacentBoxId) == -1 ) {
				$('.ms-box[data-boxid="'+adjacentBoxId+'"]').trigger( "click" );
			}
		});
	}

	// user wins
	if ( checkedBoxes.length >= 81-numBombs ) {
		$('.ms-box').addClass('no-touchy');
		stopTimer();
		showBombs('win');
		$('.ms-box.bomb, .ms-box.number').addClass('you-win');
		$('.alert-success').slideDown('fast');
		return false;
	}
}


function showBombs(reason) {
	// set correct icon (defaulting to bomb)
	var icon = 'bomb';
	if ( reason == 'win') {
		icon = 'flag';
	}

	$.each( bombs, function(_,boxId){
		$('.ms-box[data-boxid="'+boxId+'"]').addClass('bomb').html('<span><i class="fa fa-'+icon+'" aria-hidden="true"></i></span>');
	});
}


function startTimer() {
	isTimerRunning = true;
	timer = setInterval(updateTimer, 250);
	var initialTime = Date.now();

	function updateTimer() {
		totalTime = Date.now()-initialTime;

		var minutes = Math.floor(totalTime / 60000);
		var seconds = ((totalTime % 60000) / 1000).toFixed(0);

		$('#timer').html( minutes + ":" + (seconds < 10 ? '0' : '') + seconds );
	}
}


function stopTimer() {
	clearInterval(timer);
}


function boxAutoHeight() {
	var msBoxWidth = $('.ms-box').width();
	$('.ms-box').height( msBoxWidth );
}